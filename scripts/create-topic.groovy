#!/usr/bin/env groovy

@Grab('log4j:log4j:1.2.16')
@GrabExclude('io.netty:netty:3.7.0.Final')
@Grab(group='org.apache.kafka', module='kafka_2.11', version='0.10.0.1')

import kafka.admin.TopicCommand$

TopicCommand$
        .MODULE$
        .main("--create --zookeeper localhost:2181 --replication-factor 1 --partition 3 --topic raw-soap".split(" "))