package com.dll.playground

import akka.actor.ActorSystem
import com.dll.playground.bootstrap.MainModule
import com.dll.playground.business.SoapWorkflow
import com.google.inject.{Guice, Stage}
import net.codingwell.scalaguice.InjectorExtensions._

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by daniellopes on 21/09/2016.
  */
object Main extends App {
  val injector = Guice.createInjector(Stage.PRODUCTION, new MainModule)
  val system = injector.instance[ActorSystem]

  injector.instance[SoapWorkflow].start()

  sys.addShutdownHook {
    injector.instance[SoapWorkflow].stop()
    system.terminate()
    Await.ready(system.whenTerminated, 10.seconds)
  }
}
