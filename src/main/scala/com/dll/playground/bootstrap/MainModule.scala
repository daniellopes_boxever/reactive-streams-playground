package com.dll.playground.bootstrap

import com.dll.playground.akkaguice.AkkaModule
import com.dll.playground.business.SoapModule
import com.dll.playground.config.ConfigModule
import net.codingwell.scalaguice.ScalaModule

class MainModule extends ScalaModule {
  override def configure(): Unit = {
    install(new ConfigModule)
    install(new AkkaModule)
    install(new SoapModule)
  }
}
