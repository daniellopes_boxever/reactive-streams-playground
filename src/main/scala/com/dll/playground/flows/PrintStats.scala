package com.dll.playground.flows

import javax.inject.Inject

import akka.stream.FlowShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Sink}
import com.typesafe.config.Config

class PrintStats @Inject() (val config: Config) {
  val batchSize = config.getInt("soap.workflow.batch-size")

  def apply() = Flow.fromGraph(GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._
    val bcast = builder.add(Broadcast[ColorStat](2))

    bcast.out(1) ~> globalStats ~> printStats

    FlowShape(bcast.in, bcast.out(0))
  })

  def printStats = Sink.foreach{stat: ColorStat => println("Color: %s, Count: %s".format(stat.color, stat.count))}

  def globalStats = Flow[ColorStat].batch(batchSize, (stat) => GlobalStat(Map(stat.color -> stat))){ case (gstat, stat) =>
    gstat.copy(colorMap = gstat.colorMap + (stat.color -> stat))
  }.async.mapConcat { gstat =>
    val total = gstat.colorMap.foldLeft(1) { case (t, (c, s)) => t + s.count}
    gstat.colorMap.values.toList.map { stat =>
      stat.copy(total = total, percentage = (stat.count / total) * 100)
    }
  }
}
case class GlobalStat(colorMap: Map[String, ColorStat] = Map())