package com.dll.playground.flows

import javax.inject.Inject

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.dll.playground.business.SoapWorkflow.SoapContext
import com.typesafe.config.Config


class GroupByColor @Inject() (config: Config){
  val paralellism = config.getInt("soap.workflow.color-parallelism") + 1
  def apply(flow: Flow[SoapContext, ColorStat, NotUsed]) =
    Flow[SoapContext].groupBy(paralellism, partitioner).async
      .via(flow)
      .mergeSubstreamsWithParallelism(paralellism)

  def partitioner(ctx: SoapContext) =
    ctx.soap.map(_.color).getOrElse("invalid")
}
