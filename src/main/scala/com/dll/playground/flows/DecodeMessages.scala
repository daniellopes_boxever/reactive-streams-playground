package com.dll.playground.flows

import javax.inject.Inject

import akka.stream.scaladsl.Flow
import com.dll.playground.business.SoapWorkflow.{ParsingError, SoapContext}
import com.dll.playground.business.soap.Soap
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.Deserializer

import scala.util.Try

class DecodeMessages @Inject()(val deser: Deserializer[Soap]) {
  def apply() = Flow[ConsumerRecord[String, Array[Byte]]].map { msg =>
    Try(deser.deserialize(null, msg.value()))
      .map(s => SoapContext(Some(s), msg))
      .recover {
        case ex: Exception =>
          ex.printStackTrace()
          SoapContext(None, msg, Some(ParsingError(ex)))
      }.get
  }
}
