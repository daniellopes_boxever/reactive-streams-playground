package com.dll.playground.flows

import javax.inject.Inject

import akka.stream.scaladsl.Flow
import com.dll.playground.business.SoapWorkflow.SoapContext
import com.typesafe.config.Config


class ColorStats @Inject() (val config: Config) {
  val batchSize = config.getInt("soap.workflow.batch-size")

  def apply() = Flow[SoapContext].batch(batchSize, (ctx) => ColorStat(ctx.soap.map(_.color).getOrElse("invalid"), 1, List(ctx))){ case (stat, ctx) =>
    val color = ctx.soap.map(_.color).getOrElse("invalid")
    stat.copy(color = color, count = stat.count + 1, contexts = stat.contexts :+ ctx)
  }

}

case class ColorStat(color: String = "", count: Int = 0, contexts: List[SoapContext] = List(), percentage: Int = 0, total: Int = 0)