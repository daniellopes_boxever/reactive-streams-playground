package com.dll.playground.business.soap

/**
Code generated from avro schemas by scalaAvro. Do not modify.
"ALL THESE FILES ARE YOURS—EXCEPT SOAP.SCALA / ATTEMPT NO MODIFICATIONS THERE"
*/

final case class Soap(
    ref: String,
    clientKey: String,
    orderRef: String,
    color: String
  ) extends com.nitro.scalaAvro.runtime.GeneratedMessage with com.nitro.scalaAvro.runtime.Message[Soap] {
    def withRef(__v: String): Soap = copy(ref = __v)
    def withClientKey(__v: String): Soap = copy(clientKey = __v)
    def withOrderRef(__v: String): Soap = copy(orderRef = __v)
    def withColor(__v: String): Soap = copy(color = __v)
    def toMutable: org.apache.avro.generic.GenericRecord = {
      val __out__ = new org.apache.avro.generic.GenericData.Record(Soap.schema)
      __out__.put("ref", ref)
      __out__.put("clientKey", clientKey)
      __out__.put("orderRef", orderRef)
      __out__.put("color", color)
      __out__
    }
    def companion = Soap
  }
object Soap extends com.nitro.scalaAvro.runtime.GeneratedMessageCompanion[Soap] {
  implicit def messageCompanion: com.nitro.scalaAvro.runtime.GeneratedMessageCompanion[Soap] = this
  val schema: org.apache.avro.Schema =
    new org.apache.avro.Schema.Parser().parse("""{"type":"record","name":"Soap","namespace":"com.dll.playground.business.soap","fields":[{"name":"ref","type":"string"},{"name":"clientKey","type":"string"},{"name":"orderRef","type":"string"},{"name":"color","type":"string"}]}""")
  val _arbitrary: org.scalacheck.Gen[Soap] = for {
    ref <- 
    com.nitro.scalaAvro.runtime.AvroGenUtils.genAvroString
    clientKey <- 
    com.nitro.scalaAvro.runtime.AvroGenUtils.genAvroString
    orderRef <- 
    com.nitro.scalaAvro.runtime.AvroGenUtils.genAvroString
    color <- 
    com.nitro.scalaAvro.runtime.AvroGenUtils.genAvroString
  } yield Soap(
    ref = ref 
    ,
    clientKey = clientKey 
    ,
    orderRef = orderRef 
    ,
    color = color 
  )
  def fromMutable(generic: org.apache.avro.generic.GenericRecord): Soap = 
    Soap(
      ref = convertString(generic.get("ref")), 
      clientKey = convertString(generic.get("clientKey")), 
      orderRef = convertString(generic.get("orderRef")), 
      color = convertString(generic.get("color"))
    )
}
