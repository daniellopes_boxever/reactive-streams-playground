package com.dll.playground.business

import java.io.ByteArrayOutputStream
import java.util

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import com.dll.playground.business.soap.Soap
import com.dll.playground.flows.{ColorStats, DecodeMessages, GroupByColor, PrintStats}
import com.google.inject.Provides
import com.softwaremill.react.kafka._
import com.typesafe.config.Config
import net.codingwell.scalaguice.ScalaModule
import org.apache.avro.generic.GenericData.Record
import org.apache.avro.generic.{GenericDatumReader, GenericDatumWriter, GenericRecord}
import org.apache.avro.io.{DecoderFactory, EncoderFactory}
import org.apache.kafka.common.serialization._

class SoapModule extends ScalaModule {
  override def configure(): Unit = {
    bind[SoapWorkflow]
    bind[DecodeMessages]
    bind[GroupByColor]
    bind[ColorStats]
    bind[PrintStats]
  }

  @Provides
  def provideKafkaSink(props: ProducerProperties[String, Array[Byte]], system: ActorSystem): Sink[ProducerMessage[String, Array[Byte]], NotUsed] = {
    val actorRef = new ReactiveKafka().publish(props)(system)
    Sink.fromSubscriber(actorRef)
  }

  @Provides
  def provideProducerProperties(config: Config): ProducerProperties[String, Array[Byte]] = {
    val conf = config.getConfig("soap.kafka.producer")
    ProducerProperties(conf.getString("brokers"), conf.getString("topic"), new StringSerializer, new ByteArraySerializer)
  }

  @Provides
  def provideKafkaSource(props: ConsumerProperties[String, Array[Byte]]): SourceWithCommitSink[String, Array[Byte]] = {
    new ReactiveKafka().sourceWithOffsetSink(props)
  }

  @Provides
  def provideConsumerProperties(config: Config): ConsumerProperties[String, Array[Byte]] = {
    val conf = config.getConfig("soap.kafka.consumer")
    ConsumerProperties(conf.getString("brokers"), conf.getString("topic"), conf.getString("group-id"),
      new StringDeserializer, new ByteArrayDeserializer).noAutoCommit()
  }

  @Provides
  def provideSoapDeserializer(): Deserializer[Soap] = {
    new Deserializer[Soap] {
      val reader =  new GenericDatumReader[Record](Soap.schema)
      override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

      override def close(): Unit = {}

      override def deserialize(topic: String, data: Array[Byte]): Soap = {
        val gen = reader.read(null, DecoderFactory.get().binaryDecoder(data, null))
        Soap.fromMutable(gen)
      }
    }
  }

  @Provides
  def provideSoapSerializer(): Serializer[Soap] = {
    new Serializer[Soap] {
      val writer = new GenericDatumWriter[GenericRecord](Soap.schema)
      override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}
      override def serialize(topic: String, data: Soap): Array[Byte] = {
        val out = new ByteArrayOutputStream()
        writer.write(data.toMutable, EncoderFactory.get().directBinaryEncoder(out, null))
        out.flush()
        out.toByteArray
      }
      override def close(): Unit = {}
    }
  }
}
