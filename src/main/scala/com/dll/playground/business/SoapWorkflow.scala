package com.dll.playground.business

import javax.inject.{Inject, Singleton}

import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import com.dll.playground.business.SoapWorkflow.SoapContext
import com.dll.playground.business.soap.Soap
import com.dll.playground.flows._
import org.apache.kafka.clients.consumer.ConsumerRecord

@Singleton
class SoapWorkflow @Inject() (implicit val materializer: ActorMaterializer,
                              val src: Src,
                              val decodeMessages: DecodeMessages,
                              val groupByColor: GroupByColor,
                              val colorStats: ColorStats,
                              val printStats: PrintStats) {

  val workflow = src.source
                  .via(decodeMessages())
                  .via(groupByColor(colorStats()))
                  .via(printStats())
                  .via(unwrapStats())
                  .to(commitSink())

  def start() = {
    println("starting FIGHT CLUB!")
    workflow.run()
  }

  def stop() = {
    materializer.shutdown()
  }

  def commitSink() = Flow[SoapContext]
    .map(_.raw)
    .to(src.offsetCommitSink)
  def unwrapStats() = Flow[ColorStat].mapConcat(_.contexts)

}

object SoapWorkflow {
  case class SoapContext(soap: Option[Soap], raw: ConsumerRecord[String, Array[Byte]], error: Option[ContextError] = None)

  sealed trait ContextError{val ex: Exception}
  case class UnknownError(ex: Exception) extends ContextError
  case class ParsingError(ex: Exception) extends ContextError
}