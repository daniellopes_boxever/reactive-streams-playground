package com.dll.playground
import com.softwaremill.react.kafka.SourceWithCommitSink

package object business {
  type Src = SourceWithCommitSink[String, Array[Byte]]
}
