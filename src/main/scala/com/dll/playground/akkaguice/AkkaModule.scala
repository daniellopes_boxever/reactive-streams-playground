package com.dll.playground.akkaguice

import javax.inject.Inject

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.dll.playground.akkaguice.AkkaModule.{ActorMaterializerProvider, ActorSystemProvider}
import com.google.inject.{Injector, Provider}
import com.typesafe.config.Config
import net.codingwell.scalaguice.ScalaModule
import org.slf4j.LoggerFactory

object AkkaModule {
  class ActorSystemProvider @Inject() (val config: Config, val injector: Injector) extends Provider[ActorSystem] {
    override def get() = {
      val system = ActorSystem("main-actor-system", config)
      // add the GuiceAkkaExtension to the system, and initialize it with the Guice injector
      GuiceAkkaExtension(system).initialize(injector)
      system
    }
  }
  class ActorMaterializerProvider @Inject() (val system: ActorSystem) extends Provider[ActorMaterializer] {
    val log = LoggerFactory.getLogger("root")
    override def get() = {
      val decider: Supervision.Decider = {
        case ex =>
          log.error("Failure: ", ex)
          Supervision.Stop
      }
      ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))(system)
    }
  }
}

/**
  * A module providing an Akka ActorSystem.
  */
class AkkaModule extends ScalaModule {

  override def configure() {
    bind[ActorSystem].toProvider[ActorSystemProvider].asEagerSingleton()
    bind[ActorMaterializer].toProvider[ActorMaterializerProvider].asEagerSingleton()
  }
}
