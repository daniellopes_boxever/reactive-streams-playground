package com.dll.playground.config

import com.dll.playground.config.ConfigModule.ConfigProvider
import com.google.inject.Provider
import com.typesafe.config.{Config, ConfigFactory}
import net.codingwell.scalaguice.ScalaModule

object ConfigModule {
  class ConfigProvider extends Provider[Config] {
    override def get() =
      ConfigFactory.load()
  }
}

/**
  * Binds the application configuration to the [[Config]] interface.
  *
  * The com.dll.playground.config is bound as an eager singleton so that errors in the com.dll.playground.config are detected
  * as early as possible.
  */
class ConfigModule extends ScalaModule {

  override def configure() {
    bind[Config].toProvider[ConfigProvider].asEagerSingleton()
  }

}
