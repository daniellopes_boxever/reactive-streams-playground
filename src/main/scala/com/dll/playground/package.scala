package com.dll

import com.dll.playground.business.SoapWorkflow.{ContextError, SoapContext}


package object playground {
  implicit class RichContextError(error: ContextError) {
    def withContext(ctx: SoapContext) = ctx.copy(error = Some(error))
  }
}
