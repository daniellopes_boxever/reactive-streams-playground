package com.dll.playground

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, ThrottleMode}
import com.dll.playground.bootstrap.MainModule
import com.dll.playground.business.soap.Soap
import com.google.inject.{Guice, Stage}
import com.softwaremill.react.kafka.{KeyValueProducerMessage, ProducerMessage}
import net.codingwell.scalaguice.InjectorExtensions._
import org.apache.kafka.common.serialization.Serializer
import org.scalacheck.Gen

import scala.concurrent.Await
import scala.concurrent.duration._

object MakeSoap extends App {
  type SoapSink = Sink[ProducerMessage[String, Array[Byte]], NotUsed]
  val injector = Guice.createInjector(Stage.DEVELOPMENT, new MainModule)
  val system = injector.instance[ActorSystem]
  implicit val materializer = injector.instance[ActorMaterializer]
  val ser = injector.instance[Serializer[Soap]]
  val sink = injector.instance[SoapSink]

  val soapGen = for {
    ref <- Gen.alphaStr
    clientKey <- Gen.alphaStr
    orderRef <- Gen.alphaStr
    color <- Gen.oneOf(Seq("Orange", "Blue", "Red", "Green", "Black", "Yellow"))
  } yield Soap(ref, clientKey, orderRef, color)

  val soaps = Gen.listOfN(20000, soapGen).sample.get

  Source(soaps)
    .throttle(100, 2.seconds, 200, ThrottleMode.Shaping)
    .map({ soap =>
      println(soap.color)
      KeyValueProducerMessage(soap.ref, ser.serialize(null, soap))
    })
    .to(sink)
    .run()

  sys.addShutdownHook {
    system.terminate()
    Await.ready(system.whenTerminated, 60.seconds)
  }
}
