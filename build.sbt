name := "reactive-streams-playground"

version := "1.0"
scalaVersion := "2.11.8"

val akkaVersion = "2.4.10"

//libraryDependencies += "org.apache.kafka" %% "kafka" % "0.10.0.1"
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.21"
libraryDependencies += "com.softwaremill.reactivekafka" % "reactive-kafka-core_2.11" % "0.10.1"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-http-core" % akkaVersion
libraryDependencies += "net.codingwell" %% "scala-guice" % "4.1.0"
libraryDependencies += "com.gonitro" %% "avro-codegen-runtime" % "0.3.4"

libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion % "test"
libraryDependencies += "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.0" % "test"
//libraryDependencies += "net.manub" %% "scalatest-embedded-kafka" % "0.7.1" % "test"

excludeDependencies += "org.slf4j" % "log4j-over-slf4j"

scalacOptions ++= Seq(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Ywarn-dead-code",
  "-language:_",
  "-target:jvm-1.8",
  "-encoding", "UTF-8"
)
