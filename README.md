# Akka Streams Playground #

Simple Akka streams example project.
Requires Kafka 0.9

### What to do ###

* [Download Kafka 0.9](https://www.apache.org/dyn/closer.cgi?path=/kafka/0.9.0.1/kafka_2.11-0.9.0.1.tgz)
* Untar and uncomment Kafka property called **host.name** in server.properties 
* Run included Zookeeper and Kafka afterwards
* Execute com.dll.playground.MakeSoap to generate Kafka messages
* Execute com.dll.playground.Main to start processing Kafka messages
